const MultiSig = artifacts.require('MultiSig');
const UpgradeableContractProxy = artifacts.require('UpgradeableContractProxy');
const MultiSigV2 = artifacts.require('MultiSigV2');


// This test receives four different accounts to test different scenarios in the MultiSig Smart Contract
contract('MultiSig', ([notStakeholderAccount, proxyOwner, stakeholder, stakeholder2, stakeholder3]) => {
    let multiSigContractInstance = null;
    let proxy = null;
    beforeEach(async () => {

        // Here we use the UpgradableContractProxy Smart Contract to use the address of the MultiSig contract and send a few Wei
        // to use in our withdraw function
        const multiSigWithoutProxy = await MultiSig.new({from: proxyOwner});
        proxy = await UpgradeableContractProxy.new({from: proxyOwner});
        await proxy.updateImplementation(multiSigWithoutProxy.address, {from: proxyOwner});
        proxy.sendTransaction({from: proxyOwner, value: 100000000});

        // We pass the proxy address to the MultiSig contract
        multiSigContractInstance = await MultiSig.at(proxy.address);
        console.log("Proxy: " + proxy.address);
        console.log("MultiSig Contract Address: " + multiSigWithoutProxy.address);
    });


    it('should be possible to withdraw funds to a specific address when 50% of the stakeholders ' +
        'vote to approve said withdrawal of funds', async () => {


        const threshold = 1;
        const numberOfStakeholders = 2;

        await multiSigContractInstance.addStakeholder(
            stakeholder,
            {
                from: proxyOwner
            }
        );

        await multiSigContractInstance.addStakeholder(
            stakeholder2,
            {
                from: proxyOwner
            }
        );


        let thesholdFromSmartContract = await multiSigContractInstance.getThreshold({from: proxyOwner});
        assert.equal(threshold, thesholdFromSmartContract.toNumber(), "Contract threshold invalid, should not happen");

        let numberOfStakeholdersFromSmartContract = await multiSigContractInstance.getNumberOfStakeholders({from: proxyOwner});
        assert.equal(numberOfStakeholders, numberOfStakeholdersFromSmartContract.toNumber(), "Number of stakeholders is invalid!");

        let pendingTransactions = await multiSigContractInstance.pendingTransactions({
            from: proxyOwner
        });

        assert.equal(pendingTransactions.length, 0, "There should be no pending transactions!");


        const getWalletBalance = await multiSigContractInstance.contractBalance();
        console.log("wallet balance ", getWalletBalance.toNumber());

        await multiSigContractInstance.withdraw({
            from: proxyOwner,
        });


        pendingTransactions = await multiSigContractInstance.pendingTransactions({
            from: proxyOwner
        });

        assert.equal(pendingTransactions.length, 1, "There should be a pending transaction!");


        await multiSigContractInstance.signTransaction(0, {from: stakeholder});


        pendingTransactions = await multiSigContractInstance.pendingTransactions({
            from: proxyOwner
        });

        assert.equal(pendingTransactions.length, 0, "There should be no pending transactions!");

    });

    it('it should not be possible to withdraw funds if not an owner', async () => {
        const threshold = 1;
        const numberOfStakeholders = 2;


        await multiSigContractInstance.addStakeholder(
            stakeholder,
            {
                from: proxyOwner
            }
        );

        await multiSigContractInstance.addStakeholder(
            stakeholder2,
            {
                from: proxyOwner
            }
        );


        let thesholdFromSmartContract = await multiSigContractInstance.getThreshold({from: proxyOwner});
        assert.equal(threshold, thesholdFromSmartContract.toNumber(), "Contract threshold invalid, should not happen");

        let numberOfStakeholdersFromSmartContract = await multiSigContractInstance.getNumberOfStakeholders({from: proxyOwner});
        assert.equal(numberOfStakeholders, numberOfStakeholdersFromSmartContract.toNumber(), "Number of owners is invalid!");


        try {
            await multiSigContractInstance.withdraw({
                from: notStakeholderAccount,
            });
        } catch (error) {

            assert.equal(
                error,
                `Error: Returned error: VM Exception while processing transaction: revert`
            );
        }


    });

    it('it should not be possible to approve the withdrawal of funds if not an owner ', async () => {
        const threshold = 1;
        const numberOfStakeholders = 2;


        await multiSigContractInstance.addStakeholder(
            stakeholder,
            {
                from: proxyOwner
            }
        );

        await multiSigContractInstance.addStakeholder(
            stakeholder2,
            {
                from: proxyOwner
            }
        );


        let thesholdFromSmartContract = await multiSigContractInstance.getThreshold({from: proxyOwner});
        assert.equal(threshold, thesholdFromSmartContract.toNumber(), "Contract threshold invalid, should not happen");

        let numberOfStakeholdersFromSmartContract = await multiSigContractInstance.getNumberOfStakeholders({from: proxyOwner});
        assert.equal(numberOfStakeholders, numberOfStakeholdersFromSmartContract.toNumber(), "Number of owners is invalid!");


        await multiSigContractInstance.withdraw({
            from: proxyOwner,
        });


        try {
            await multiSigContractInstance.signTransaction(0, {from: notStakeholderAccount});
        } catch (error) {

            assert.equal(
                error,
                `Error: Returned error: VM Exception while processing transaction: revert`
            );
        }

    });

    it('it should not be possible to disapprove the withdrawal of funds if not an owner', async () => {
        const threshold = 1;
        const numberOfStakeholders = 2;


        await multiSigContractInstance.addStakeholder(
            stakeholder,
            {
                from: proxyOwner
            }
        );

        await multiSigContractInstance.addStakeholder(
            stakeholder2,
            {
                from: proxyOwner
            }
        );


        let thesholdFromSmartContract = await multiSigContractInstance.getThreshold({from: proxyOwner});
        assert.equal(threshold, thesholdFromSmartContract.toNumber(), "Contract threshold invalid, should not happen");

        let numberOfStakeholdersFromSmartContract = await multiSigContractInstance.getNumberOfStakeholders({from: proxyOwner});
        assert.equal(numberOfStakeholders, numberOfStakeholdersFromSmartContract.toNumber(), "Number of stakeholders is invalid!");


        await multiSigContractInstance.withdraw({
            from: proxyOwner,
        });

        await multiSigContractInstance.signTransaction(0, {from: stakeholder});


        try {
            await multiSigContractInstance.unsignTransaction(0, {from: notStakeholderAccount});
        } catch (error) {

            assert.equal(
                error,
                `Error: Returned error: VM Exception while processing transaction: revert`
            );
        }

    });

    it('it should be possible for a stakeholder to disapprove a transaction after approving it', async () => {
        const threshold = 2;
        const numberOfStakeholders = 3;


        await multiSigContractInstance.addStakeholder(
            stakeholder,
            {
                from: proxyOwner
            }
        );

        await multiSigContractInstance.addStakeholder(
            stakeholder2,
            {
                from: proxyOwner
            }
        );

        await multiSigContractInstance.addStakeholder(
            stakeholder3,
            {
                from: proxyOwner
            }
        );


        let thesholdFromSmartContract = await multiSigContractInstance.getThreshold({from: proxyOwner});
        assert.equal(threshold, thesholdFromSmartContract.toNumber(), "Contract threshold invalid, should not happen");

        let numberOfStakeholdersFromSmartContract = await multiSigContractInstance.getNumberOfStakeholders({from: proxyOwner});
        assert.equal(numberOfStakeholders, numberOfStakeholdersFromSmartContract.toNumber(), "Number of stakeholders is invalid!");


        await multiSigContractInstance.withdraw({
            from: proxyOwner,
        });

        const numberOfConfirmationsBeforeSigningTransaction = await multiSigContractInstance.getTransactionVoteCount(0, {from: stakeholder});

        await multiSigContractInstance.signTransaction(0, {from: stakeholder});
        await multiSigContractInstance.unsignTransaction(0, {from: stakeholder});

        const numberOfConfirmationsAfterSigningTransaction = await multiSigContractInstance.getTransactionVoteCount(0, {from: stakeholder});

        assert.equal(numberOfConfirmationsBeforeSigningTransaction.toNumber(), numberOfConfirmationsAfterSigningTransaction.toNumber(),
            "Transaction votes should be the same!");
    });

    it('it should be possible to upgrade the MultiSig contract to MultiSigV2', async () => {

        // In this test, we test if the MultiSig smart contract can or not be upgraded to a MultiSig version 2.
        // We add stakeholders to the MultiSig version 1 and calculate the threshold.

        const thresholdFromOldImplementation = 1;
        const numberOfStakeholdersInPreviousImplementation = 2;


        await multiSigContractInstance.addStakeholder(
            stakeholder,
            {
                from: proxyOwner
            }
        );

        await multiSigContractInstance.addStakeholder(
            stakeholder2,
            {
                from: proxyOwner
            }
        );

        // Now we point the Proxy to use the address of the MultiSigV2 and see if we retain storage
        const multiSigV2_withoutProxy = await MultiSigV2.new({from: proxyOwner});

        console.log('###### -> Upgraded implementation to MultiSigV2 at ' + multiSigV2_withoutProxy.address);
        await proxy.updateImplementation(multiSigV2_withoutProxy.address, {from: proxyOwner});

        const currentImplementation = await proxy.implementation({from: proxyOwner});
        assert.equal(currentImplementation, multiSigV2_withoutProxy.address);

        const multiSigV2 = await MultiSigV2.at(proxy.address);


        // From here on, we compare the previous values in MultiSigV1 and see that they match the values of MultiSigV2.
        // We have achieved smart contract upgradability.
        console.log('###### -> Getting the number of stakeholders at: ' + multiSigV2.address);
        const numberOfStakeholdersInNewImplementation = await multiSigV2.viewNumberOfStakeholders({from: proxyOwner});


        assert.equal(numberOfStakeholdersInPreviousImplementation, numberOfStakeholdersInNewImplementation, "Upgrade failed. " +
            "The  number of stakeholders should be equal!");

        const thresholdFromNewImplementation = await multiSigV2.getThreshold({from: proxyOwner});
        assert.equal(thresholdFromOldImplementation, thresholdFromNewImplementation.toNumber(),
            "Upgrade failed. Threshold should be equal.");

        await multiSigV2.incrementUpgradeVariable({from: proxyOwner});

        const upgradeVariable = await multiSigV2.getIncrementedUpgradeVariable({from: proxyOwner});
        assert.equal(upgradeVariable.toNumber(), 1, "Upgrade failed. Should have incremented.");
    });
});

