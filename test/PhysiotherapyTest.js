const Physiotherapy = artifacts.require("./Physiotherapy");
const assert = require("assert");

const manuelaPrivateKey = '0xf7cbd9813625157b42de1068b1d96bd7e5324821e3db384228226c0a752fdbc';


const workers = [
    {
        workerPrivateKey: '0x747dd4a71a90959b49a6ebd6513d9391c25feaf77f8e62baa2805fc62374734b',
        workerPublicKey: '0xb1eda6b3f09128920a3556ed6ea462dcd16b3c29c66dc0c1ff3ae617f7452b98',
        funds: 1000

    },
    {
        workerPrivateKey: '0xee6bbf00b93a50d1eb98d3c52ea4afb0714408adaa2e0ab052f0378569a6698e',
        workerPublicKey: '0xb6342b3237335cd545453703193afff2d93fdd06594d094139a409ae3fe2229f',
        funds: 1000
    }
];

const workerActivities =
    [
        {
            publicKey: workers[workers.length - 1].workerPublicKey,
            startDate: new Date().toString(),
            endDate: new Date().toString(),
            description: 'Ultra sound',
            activityType: "0",
        },

        {
            publicKey: workers[workers.length - 1].workerPublicKey,
            startDate: new Date().toString(),
            endDate: new Date().toString(),
            description: 'Current',
            activityType: "1",
        },
        {
            publicKey: workers[workers.length - 1].workerPublicKey,
            startDate: new Date().toString(),
            endDate: new Date().toString(),
            description: 'Aerossol',
            activityType: "2",
        }

    ];


contract("Physiotherapy", accounts => {
    let physiotherapyContractInstance;
    beforeEach(async () => {
        physiotherapyContractInstance = await Physiotherapy.deployed();
    });


    it("should be possible for only Manuela to add workers"

        , async () => {

            for (let i = 0; i < workers.length; i++) {
                await physiotherapyContractInstance.addWorker(
                    manuelaPrivateKey,
                    workers[i].workerPrivateKey,
                    workers[i].workerPublicKey,
                    workers[i].funds, {
                        from: accounts[0]
                    }
                );
            }


            let getWorkerData = await physiotherapyContractInstance.getWorkerAtIndex(
                workers.length - 1, manuelaPrivateKey
            );


            const privateKeyFetchedFromSmartContract = getWorkerData.logs[0].args[0];
            const publicKeyFetchedFromSmartContract = getWorkerData.logs[0].args[1];
            const fundsFetchedFromSmartContract = getWorkerData.logs[0].args[2].toNumber();


            const workerDataFetchedFromSmartContract = {
                workerPrivateKey: privateKeyFetchedFromSmartContract,
                workerPublicKey: publicKeyFetchedFromSmartContract,
                funds: fundsFetchedFromSmartContract,
            };

            assert.equal(
                JSON.stringify(workers[workers.length - 1]),
                JSON.stringify(workerDataFetchedFromSmartContract),
                "There was an error adding workers to the Blockchain"
            );

        });


    it("should be possible for only workers to add activities to them"

        , async () => {

            for (let i = 0; i < workerActivities.length; i++) {
                await physiotherapyContractInstance.addActivityToWorker(
                    workers.length - 1, // worker index
                    workers[workers.length - 1].workerPrivateKey,
                    workers[workers.length - 1].workerPublicKey,
                    workerActivities[i].activityType,
                    workerActivities[i].startDate,
                    workerActivities[i].endDate,
                    workerActivities[i].description,
                    {
                        from: accounts[0]
                    }
                );

                let getWorkerData = await physiotherapyContractInstance.getWorkerActivityAtIndex(
                    workers.length - 1, // worker index
                    i
                );


                const workerDataFetchedFromSmartContract = {
                    publicKey: getWorkerData[0],
                    startDate: getWorkerData[1],
                    endDate: getWorkerData[2],
                    description: getWorkerData[3],
                    activityType: getWorkerData[4],
                };


                assert.equal(
                    JSON.stringify(workerDataFetchedFromSmartContract),
                    JSON.stringify(workerActivities[i]),
                    "There was an error adding activities to a worker in the Blockchain"
                );

            }

        });


    it("should be possible for only Manuela to delete workers"

        , async () => {

            const workersLength = await physiotherapyContractInstance.getWorkersLength(
            );

            const workersLengthString = (JSON.stringify(workersLength));

            await physiotherapyContractInstance.removeWorkerAtIndex(
                workers.length - 1,
                manuelaPrivateKey,
                {
                    from: accounts[0]
                }
            );


            let workersLengthAfterDeletion = await physiotherapyContractInstance.getWorkersLength(
                {
                    from: accounts[0]
                }
            );

            const workersLengthAfterDeletionString = (JSON.stringify(workersLengthAfterDeletion));

            assert.notEqual(
                workersLengthString,
                workersLengthAfterDeletionString,
                "Something is wrong, there should be ONLY one added worker in the Blockchain :("
            );


        });


});




