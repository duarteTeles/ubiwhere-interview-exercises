pragma solidity ^0.5.0;

import "./Ownable.sol";

contract UpgradeableContractProxy is Ownable {

    /**
* @title A Proxy Smart Contract that leverages the delegateCall opcode execute code to point to a new Smart Contract address (the new implementation)
* @author Duarte Teles, March 2020
* @notice This is a Smart Contract that uses complex assembly code to be able to point to a new address on demand
* @dev This takes leverage of the delegatecall opcode. Based on https://medium.com/quillhash/how-to-write-upgradable-smart-contracts-in-solidity-d8f1b95a0e9a
*/

    // The address of the current Smart Contract implementation
    address private _currentImplementation;

    // Only the contract owner can point to new Smart Contract addresses (new implementations)
    constructor() public Ownable() {
    }

    /**
    * @author Duarte Teles
    * @notice A function that allows the Proxy Smart Contract to point to a new Smart Contract address (new implementation)
    * @dev This function allows to the Proxy Smart Contract to point to a new one, and assigns the _currentImplementation to be the new Smart Contract
    * address (new implementation). Only the contract creator can point to new addresses
    */
    function updateImplementation(address _newImplementation) onlyOwner public {
        require(_newImplementation != address(0));

        _currentImplementation = _newImplementation;
    }

    /**
    * @author Duarte Teles
    * @notice A read-only function to view the address of the current implementation (current Smart Contract address)
    * @return The contract address of the current implementation
    */
    function implementation() public view returns (address) {
        return _currentImplementation;
    }

    /**
    * @author Duarte Teles
    * @notice The default fallback function that uses complex assembly code to leverage the delegatecall opcode
    * @dev A major drawback of this approach is that this function is reserved to leverage the delegatecall opcode.
    * For this reason, Ether cannot be send through this default fallback function. A special one must be created for that effect
    */
    function() payable external {
        address _impl = implementation();
        require(_impl != address(0));

        assembly {
            let ptr := mload(0x40)
            calldatacopy(ptr, 0, calldatasize)
            let result := delegatecall(gas, _impl, ptr, calldatasize, 0, 0)
            let size := returndatasize
            returndatacopy(ptr, 0, size)

            switch result
            case 0 {revert(ptr, size)}
            default {return (ptr, size)}
        }
    }
}
