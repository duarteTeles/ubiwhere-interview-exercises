# Introduction

This document describes the process of developing Ubiwhere's Internview Exercise 2.2: a multiSig wallet solution using Ethereum Smart Contracts with two different versions to test Ethereum Smart Contract Upgradability. This is detailed in the chapter **Blockchain Software Engineering (Simplified)**, where all 
the most relevant software development phases are documented, albeit superficially.

# Blockchain Software Engineering (Simplified)

This chapter focuses on the software development process of Exercise 2.2 and the various
software phases required to develop safe, bug-free Ethereum Smart Contracts. These are:

1. **Requirement gathering and analysis**
2. **Design**
3. **Implementation**
4. **Testing**
5. **Deployment**

## Requirement Gathering and Analysis

This is the phase where system requirements are gathered an analyzed for the system/problem in question with the creation of multiple diagrams such as UML Use-Cases or Classes diagrams. Here, it will only be discussed the multiple functional requirements for Ubiwhere's Interview Exercise 2.2. 

### Functional Requirements

The functional requirements for this exercise are as follows:

1. Only the contract owner can add/remove stakeholders
2. The stakeholders can request to withdraw funds from the contract which are then pending approval from other stakeholders
3. A stakeholder must approve a transaction by voting/validating/signing it
4. A stakeholder cannot vote/validate/sign a transaction more than once
5. A stakeholder can revoke his vote/validate/signature for a given transaction
6. Funds are only withdrawn if 50% or more of the stakeholders voted/validated/signed a transaction (a threshold)
7. Once the threshold is met, all of the contract funds are transferred to address **0x357573E1b99293Bc09b7392B560b3C336c22690C**
8. The MultiSig Smart Contract must be upgradable to a new version

### Contracts Diagram

The chosen implementation has the following contracts diagram:

![The Contracts Diagram](./docs/images/MultiSigContractsDiagram.sol.svg  "The Contracts Diagram")

The main Smart Contract, **MultiSig.sol** inherits from the **Ownable** contract
and uses the **SafeNath** library, both by OpenZepplin (https://openzeppelin.com/contracts/).

In the diagram are also visible the structs and functions it has.

### Design and Implementation

The design and implementation phase of developing software is where multiple aspects of this process are
defined such as technologies/frameworks to use, the folder structure of the application, etc.

In Exercise's 2.2 case, the Solidity version 0.5.0 was chosen, in addition to Truffle  version 5.0.0 and NodeJS version 11.15.0. The core Smart Contract, **MultiSig.sol** was developed with no optimization in mind.

### Testing

Testing must be taken seriously when developing decentralized applications with Ethereum.For this purpose, in the **test** folder, a **MultiSigUpgrade.js** unit test was developed using Mocha syntax. Its test cases are the following:

1. It should be possible to withdraw funds to a specific address when 50% of the stakeholders vote to approve said withdrawal of funds: In this test, first two stakeholders are added by the contract owner. Then, the contract's account balance is withdrawn
from a valid stakeholder and finally we see if the pendingTransactions variable is 0, among other things. If everything passes, then the test passes
2. It it should not be possible to withdraw funds if not an owner: in this test, the **withdraw** function is invoked by an invalid stakeholder. The test passes if the transaction fails 3. It should not be possible to approve the withdrawal of funds if not an owner: this test is similar to the test above,
but instead of withdrawing funds, it tests if an address that is not a stakeholder cannot withdraw funds. Test passes if the withdraw transaction fails
4. It should not be possible to disapprove the withdrawal of funds if not an owner: in this test, the contract owner cannot approve/disapprove transactions. If the invocation of the **unsignTransaction** function fails, the test passes 
5. It should be possible for a stakeholder to disapprove a transaction after approving it: in this test, after a stakeholder approves a transaction, he/her should be able to disapprove/unsign it. This test is successful if the invocation of the **unsignTransaction** is successful
6. It should be possible to upgrade the MultiSig contract to MultiSigV2: this test attempts to test the upgradability of Ethereum Smart Contracts by using a Proxy and the EVM's delegatecall opcode.  The test passes if variables from a new MultiSigV2 implementation are read preserving the values from the previous MultiSig implementation 

### Deployment

The deployment phase is one of the most important ones in Ethereum Smart Contract development. However, due to time limitations, the Smart Contracts in this exercise were only deployed to a local test Blockchain: Ganache by using Truffle's migrations.

## Smart Contract Upgradability

This **MultiSig.sol** implementation proves that upgradability in Ethereum is possible until a certain extent. A special Smart Contract **UpgradableContractProxy.sol** is used to leverage the EVM's delegatecall opcode and point to a new implementation of a Smart Contract while preserving data from a previous implementation. More information on this process is available at:
https://medium.com/quillhash/how-to-write-upgradable-smart-contracts-in-solidity-d8f1b95a0e9a.

## Smart Contract Documentation

Smart Contract documentation about their methods, functions, etc can be found at the docs folder
in each exercise. For the main Smart Contract, this is  [MultiSig.md](./docs/MultiSig.md) or [MultiSig Alternative](./docs/MultiSigAlternative.md). For the upgraded version of the MultiSig Smart Contract, this is: , [MultiSigV2.md](./docs/MultiSigV2.md) or [MultiSigV2Alternative.md](./docs/MultiSigV2Alternative.md) and [UpgradableContractProxy.md](./docs/UpgradeableContractProxy.md) or [UpgradableContractProxyAlternative](./docs/UpgradeableContractProxyAlternative.md)  for the Proxy Smart Contract.

## Limitations

The main limitations are on what the Proxy Smart Contract can bring us with its upgradability. 

## Other Methods of Upgrading Smart Contracts

The OpenZeppelin team developed the OpenZeppelin command-line tools: https://docs.openzeppelin.com/cli/2.8/getting-started#setting-up-your-project
which includes a way to upgrade Ethereum Smart Contracts that are similar to what it was developed for this Exercise 2.2: https://docs.openzeppelin.com/upgrades/2.7/creating-upgradeable-from-solidity.

# Conclusion

This document documented briefly the whole software development process for Ubiwhere's Interview Exercise 2.2,  as well as its limitations, extra documentation and proves that Ethereum
Smart Contract upgradability, albeit not trivial, is possible and feasible. 