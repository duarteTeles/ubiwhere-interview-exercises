pragma solidity ^0.5.0;


/**
 * @title Ownable - by OpenZeppelin
 * @dev The Ownable contract has an owner address, and provides basic authorization control
 * functions, this simplifies the implementation of "user permissions".
 */

contract Ownable {
    address public owner;

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor() public {
        owner = msg.sender;
    }

    /**
       * @dev Throws if called by any account other than the owner.
       */
    modifier onlyOwner() {
        require(isOwner(), "Ownable: caller is not the owner");
        _;
    }

    /**
     * @dev Returns true if the caller is the current owner.
     */
    function isOwner() public view returns (bool) {
        return msg.sender == owner;
    }


}