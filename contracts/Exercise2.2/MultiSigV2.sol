pragma solidity ^0.5.0;


import "./MultiSig.sol";

/**
* @title Version 2 of a simple MultiSig Ethereum Smart Contract. This contract tests the upgradability of Smart Contracts
* @author Duarte Teles, March 2020
* @notice Simple Smart Contract to test its upgradability
* @dev This contract adds additional logic to the MultiSig Smart Contract while maintaining storage of Version 1 (meaning for example the number of stakeholders carry down to this version)
*/
contract MultiSigV2 is MultiSig {

    /**
    * @author Duarte Teles
    * @notice A new function to view the number of stakeholders safely stored in the previous version of the MultiSig Smart Contract
    * @return The number of stakeholders that carried from the previous version of the MultiSig Smart Contract to this version
    */
    function viewNumberOfStakeholders() public view returns (uint256)
    {
        return numberOfStakeholders;
    }

    /**
    * @author Duarte Teles
    * @notice A function that increments a useless variable in the previous implementation, only to prove that upgrading Smart Contracts is possible
    */

    function incrementUpgradeVariable() public
    {
        upgradeVariable++;
    }

    /**
    * @author Duarte Teles
    * @notice A read-only function to view the value of upgradeVariable
    * @return The value of upgradeVariable
*/
    function getIncrementedUpgradeVariable() public view returns (uint256)
    {
        return upgradeVariable;
    }
}