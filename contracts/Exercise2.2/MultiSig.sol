pragma solidity ^0.5.0;


import './SafeMath.sol';

/**
* @title Version 1 of a simple MultiSig Ethereum Smart Contract
* @author Duarte Teles, March 2020
* @notice Use this contract as a simple multisig wallet implementation. For more advance usages, consider
* other implementations such as the Consensys MultiSig Wallet
* @dev Use this contract only as a basic multisig wallet implementation with upgrade capabilities to new versions
*/
contract MultiSig {

    // The contract owner
    address private _owner;

    // A special mapping to indicate if an address is a stakeholder or not (0 if an address is not a stakeholder, 1 if it is)
    mapping(address => uint8) private _stakeholders;

    // A mapping that maps a transaction index (0,1,2,...) to a specific transaction
    mapping(uint => Transaction) private _transactions;

    // The transaction index (0,1,2,...)
    uint private _transactionIndex;

    // The address to withdraw funds to
    address payable  addressToSendFundsTo = 0x357573E1b99293Bc09b7392B560b3C336c22690C;

    // The minimal amount of votes required to approve a transaction (50% minimum)
    uint public threshold;

    uint public numberOfStakeholders;

    // A useless variable to test only the upgradability of this contract
    uint public upgradeVariable = 0;

    // An array of pending transactions: when a a stakeholder requests a transaction (for example withdraws funds), the transaction index gets stored here
    // waiting to be confirmed/validated by another stakeholder
    uint[] private _pendingTransactions;

    // A struct with data about a transaction
    struct Transaction {
        // Who is calling the transaction
        address from;
        // To whom the transaction will be sent
        address payable to;
        // The amount in the transaction
        uint amount;
        // The number of valid stakeholder votes so far
        uint8 voteCount;

        // A mapping that stores a flag (0 or 1) for a specific address to see if it has already confirmed or unconfirmed a transaction (signed or unsigned a transaction)
        mapping(address => uint8) voted;
    }

    // Events
    event ThresholdEvent(uint);
    event TransactionData(string, address);
    event TransactionSignatureCount(string, uint);
    event DepositFunds(address from, uint amount);
    event TransactionCreated(address from, address to, uint amount, uint transactionId);
    event TransactionCompleted(address from, address to, uint amount, uint transactionId);
    event TransactionSigned(address by, uint transactionId);
    event TransactionUnsigned(address by, uint transactionId);

    modifier isOwner() {
        require(msg.sender == _owner);
        _;
    }

    // Modifier to see if an address ia a valid stakeholder (flag must be 1)
    modifier validStakeholder() {
        require(msg.sender == _owner || _stakeholders[msg.sender] == 1);
        _;
    }

    // The contract's constructor. Initializes the owner, the threshold and number of stakeholders
    constructor () payable
    public {
        _owner = msg.sender;
        threshold = 0;
        numberOfStakeholders = 0;
    }


    /**
    * @author Duarte Teles
    * @notice Default fallback function that emits an event and displays the address of the sender and the value passed
    *
    */
    function()
    external
    payable {
        emit  DepositFunds(msg.sender, msg.value);
    }

    /**
   * @author Duarte Teles
   * @notice This function allows for the contract owner to add new stakeholders in addition to calculating the minimum number of votes required to validate transactions (threshold).
   * @dev This function uses OpenZeppelin's SafeMath library to safely calculate the amount of minimum votes required to approve a transaction
   * If the number of stakeholders are odd, then half is calculated by dividing the number of stakeholders and adding 1. For example, in 3 stakeholders,
   * the number of votes required to trigger a transaction is 3/2+1 = 2. If the number of stakeholders is even, then it's a normal division by 2 to calculate
   * the minimum number of votes required to approve a transaction. Also, only the contract creator can add stakeholders: modifier isOwner
   * @param stakeholder The stakeholder address to be added
   */
    function addStakeholder(address stakeholder)
    isOwner
    public {

        _stakeholders[stakeholder] = 1;
        numberOfStakeholders++;

        if (SafeMath.mod(numberOfStakeholders, 2) == 0)
        {
            threshold = SafeMath.div(numberOfStakeholders, 2);
        }
        else
        {
            threshold = SafeMath.div(numberOfStakeholders, 2) + 1;
        }
        emit ThresholdEvent(threshold);

    }

    /**
   * @author Duarte Teles
   * @notice This function allows the contract caller to withdraw all the funds from the contract pending approval of one or more stakeholders (depending on the threshold value)
   * @dev This function registers the transaction in a pending transactions array waiting for approval of a minimal amount of stakeholders (threshold)
   */
    function withdraw() validStakeholder
    public {

        // This contract's balance
        uint contractBalance = address(this).balance;

        require(address(this).balance >= 0);

        // The transaction index
        uint transactionId = _transactionIndex++;

        // Memory initialization of a transaction
        Transaction memory transaction;
        transaction.from = msg.sender;
        transaction.to = addressToSendFundsTo;
        transaction.amount = contractBalance;
        transaction.voteCount = 0;

        // Saving this transaction
        _transactions[transactionId] = transaction;
        // Sending the transaction to the pending transactions array to be later validated by one or more stakeholders
        _pendingTransactions.push(transactionId);

        // Transaction created event
        emit TransactionCreated(msg.sender, addressToSendFundsTo, contractBalance, transactionId);
    }

    /**
   * @author Duarte Teles
   * @notice This function allows to view the transaction indices that are pending validation/confirmation from a minimum number of stakeholders
   * @return The transaction indices that are pending validation/confirmation from a minimal number of stakeholders
   */
    function pendingTransactions()
    view
    validStakeholder
    public
    returns (uint[] memory) {
        return _pendingTransactions;
    }

    /**
    * @author Duarte Teles
    * @notice This function allows for a valid shareholder to sign/validate/confirm a specific transaction. A valid shareholder cannot sign a transaction more than once
    * @dev This function increments the number of stakeholder votes and checks if it equal or greater than the threshold. If it is,
    * it checks for valid contract balance and transfers all
    * the funds from this contract to a fixed destination address and adds/removes a transaction from the pendingTransactions array. Note that in order to delete from this array
    * In solidity we need to do something similar to JavaScript splice
    * @param transactionId The transaction ID where a shareholder will sign/validate/confirm it
    */
    function signTransaction(uint transactionId)
    validStakeholder
    public {

        // The transaction we want to sign
        Transaction storage transaction = _transactions[transactionId];

        // Transaction must exist
        require(address(0x0) != transaction.from);
        // Contract creator cannot sign/validate/confirm a transaction; only a valid stakeholder can
        require(msg.sender != transaction.from);
        // A stakeholder cannot sign/validate/confirm a transaction more than once
        require(transaction.voted[msg.sender] != 1);

        // Mark that the stakeholder has voted on that transaction
        transaction.voted[msg.sender] = 1;
        // Increase the vote count for the transaction
        transaction.voteCount++;

        emit TransactionSignatureCount("Signature Count", transaction.voteCount);
        emit TransactionSignatureCount("signatures", transaction.voted[msg.sender]);

        emit TransactionSigned(msg.sender, transactionId);
        emit ThresholdEvent(threshold);

        // Check whether we have enough votes from the stakeholders to proceed with the transaction
        if (transaction.voteCount >= threshold) {
            // Check if we have a valid balance to proceed with the transaction
            require(address(this).balance >= transaction.amount);
            // Transfer al the funds to the destination address
            transaction.to.transfer(transaction.amount);
            emit TransactionCompleted(transaction.from, transaction.to, transaction.amount, transactionId);

            // After completing the transaction, delete it from the transactions mapping, in addition to deleting it from the _pendingTransactions array
            // To delete from arrays in solidity, the equivelent of JavaScript splice is done here
            uint8 replace = 0;
            for (uint i = 0; i < _pendingTransactions.length; i++) {
                if (replace == 1) {
                    _pendingTransactions[i - 1] = _pendingTransactions[i];
                } else if (transactionId == _pendingTransactions[i]) {
                    replace = 1;
                }
            }
            // Deleting both the transactions from the transactionsPending array and the mapping of transactions
            delete _pendingTransactions[_pendingTransactions.length - 1];
            _pendingTransactions.length--;
            delete _transactions[transactionId];
        }
    }

    /**
    * @author Duarte Teles
    * @notice This function allows for a valid shareholder to unsign/invalidate/unconfirm a specific transaction. A valid shareholder cannot unsign a transaction more than once
    * @param transactionId The transaction ID where a shareholder will unsign/invalidate/unconfirm it
 */
    function unsignTransaction(uint transactionId)
    validStakeholder
    public {

        // The transaction to be unsigned
        Transaction storage transaction = _transactions[transactionId];

        // The transaction must only be unsiged IF the minimal number of votes from stakeholders (threshold) is not achieved yet.
        // Otherwise, the transaction will go through, get deleted and then an error will be thrown when trying to unsign an invalid transaction
        if (transaction.voteCount < threshold)
        {
            // Contract creator cannot unsign/invalidate/unconfirm a transaction; only a valid stakeholder can
            require(msg.sender != transaction.from);

            // A valid stakeholder cannot unsign/invalidate/unconfirm a transaction more than once
            require(transaction.voted[msg.sender] != 0);

            // Remove that the stakeholder has voted this transaction
            transaction.voted[msg.sender] = 0;
            // Decrease the total number of votes for this transaction
            transaction.voteCount--;

            emit TransactionUnsigned(msg.sender, transactionId);
            emit ThresholdEvent(threshold);
        }
    }

    /**
    * @author Duarte Teles
    * @notice A function to get the minimal number of votes required from stakeholders to approve a transaction (threshold)
    * @return The threshold value
    *
    */
    function getThreshold() view public returns (uint256)
    {
        return threshold;
    }
    /**
    * @author Duarte Teles
    * @notice A read-only function that returns the balance of this contract
    * @return This contract's balance
    */
    function contractBalance()
    view
    public
    returns (uint) {
        return address(this).balance;
    }

    /**
   * @author Duarte Teles
   * @notice A read-only function that returns the total number of valid stakeholders
   * @return The total number of valid stakeholders
   */
    function getNumberOfStakeholders() public view returns (uint256)
    {
        return numberOfStakeholders;
    }

    /**
    * @author Duarte Teles
    * @notice A function that returns the total vote count for a specific transaction
    * @param transactionId The transaction ID to see how many votes it has
    * @return  The total number of votes a transaction has
    */
    function getTransactionVoteCount(uint transactionId) public validStakeholder view returns (uint256)

    {
        uint256 voteCount = _transactions[transactionId].voteCount;
        return voteCount;

    }

}