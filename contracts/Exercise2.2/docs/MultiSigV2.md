# Version 2 of a simple MultiSig Ethereum Smart Contract. This contract tests the upgradability of Smart Contracts (MultiSigV2.sol)

View Source: [contracts/Exercise2.2/MultiSigV2.sol](contracts/Exercise2.2/MultiSigV2.sol)

**↗ Extends: [MultiSig](contracts/Exercise2.2/docs/docs/MultiSig.md)**

**MultiSigV2**

Simple Smart Contract to test its upgradability

## Functions

- [viewNumberOfStakeholders()](#viewnumberofstakeholders)
- [incrementUpgradeVariable()](#incrementupgradevariable)
- [getIncrementedUpgradeVariable()](#getincrementedupgradevariable)

### viewNumberOfStakeholders

A new function to view the number of stakeholders safely stored in the previous version of the MultiSig Smart Contract

```js
function viewNumberOfStakeholders() public view
returns(uint256)
```

**Returns**

The number of stakeholders that carried from the previous version of the MultiSig Smart Contract to this version

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### incrementUpgradeVariable

A function that increments a useless variable in the previous implementation, only to prove that upgrading Smart Contracts is possible

```js
function incrementUpgradeVariable() public nonpayable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### getIncrementedUpgradeVariable

A read-only function to view the value of upgradeVariable

```js
function getIncrementedUpgradeVariable() public view
returns(uint256)
```

**Returns**

The value of upgradeVariable

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

## Contracts

* [Migrations](contracts/Exercise2.2/docs/docsracts/Exercise2.2/docs/docs/Migrations.md)
* [MultiSig](contracts/Exercise2.2/docs/docs/MultiSig.md)
* [MultiSigV2](contracts/Exercise2.2/docs/docsracts/Exercise2.2/docs/docs/MultiSigV2.md)
* [Ownable](contracts/Exercise2.2/docs/docsracts/Exercise2.2/docs/docs/Ownable.md)
* [Physiotherapy](contracts/Exercise2.1/docs/PhysiotherapySmartContract.md)
* [SafeMath](contracts/Exercise2.2/docs/docsracts/Exercise2.2/docs/docs/SafeMath.md)
* [StringUtils](contracts/Exercise2.1/docs/StringUtils.md)
* [UpgradeableContractProxy](contracts/Exercise2.2/docs/docsracts/Exercise2.2/docs/docs/UpgradeableContractProxy.md)
