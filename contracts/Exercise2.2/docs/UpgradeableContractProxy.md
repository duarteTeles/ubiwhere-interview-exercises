# UpgradeableContractProxy.sol

View Source: [contracts/Exercise2.2/UpgradeableContractProxy.sol](contracts/Exercise2.2/UpgradeableContractProxy.sol)

**↗ Extends: [Ownable](contracts/Exercise2.2/docs/docsracts/Exercise2.2/docs/docs/Ownable.md)**

**UpgradeableContractProxy**

## Contract Members
**Constants & Variables**

```js
address private _currentImplementation;

```

## Functions

- [()](#)
- [updateImplementation(address _newImplementation)](#updateimplementation)
- [implementation()](#implementation)
- [()](#)

### 

```js
function () public nonpayable Ownable 
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### updateImplementation

A function that allows the Proxy Smart Contract to point to a new Smart Contract address (new implementation)

```js
function updateImplementation(address _newImplementation) public nonpayable onlyOwner 
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _newImplementation | address |  | 

### implementation

A read-only function to view the address of the current implementation (current Smart Contract address)

```js
function implementation() public view
returns(address)
```

**Returns**

The contract address of the current implementation

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### 

The default fallback function that uses complex assembly code to leverage the delegatecall opcode

```js
function () external payable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

## Contracts

* [Migrations](contracts/Exercise2.2/docs/docsracts/Exercise2.2/docs/docs/Migrations.md)
* [MultiSig](contracts/Exercise2.2/docs/docs/MultiSig.md)
* [MultiSigV2](contracts/Exercise2.2/docs/docsracts/Exercise2.2/docs/docs/MultiSigV2.md)
* [Ownable](contracts/Exercise2.2/docs/docsracts/Exercise2.2/docs/docs/Ownable.md)
* [Physiotherapy](contracts/Exercise2.1/docs/PhysiotherapySmartContract.md)
* [SafeMath](contracts/Exercise2.2/docs/docsracts/Exercise2.2/docs/docs/SafeMath.md)
* [StringUtils](contracts/Exercise2.1/docs/StringUtils.md)
* [UpgradeableContractProxy](contracts/Exercise2.2/docs/docsracts/Exercise2.2/docs/docs/UpgradeableContractProxy.md)
