# Version 1 of a simple MultiSig Ethereum Smart Contract (MultiSig.sol)

View Source: [contracts/Exercise2.2/MultiSig.sol](contracts/Exercise2.2/MultiSig.sol)

**↘ Derived Contracts: [MultiSigV2](contracts/Exercise2.2/docs/MultiSigV2.md)**

**MultiSig**

Use this contract as a simple multisig wallet implementation. For more advance usages, consider
other implementations such as the Consensys MultiSig Wallet

## Structs
### Transaction

```js
struct Transaction {
 address from,
 address payable to,
 uint256 amount,
 uint8 voteCount,
 mapping(address => uint8) voted
}
```

## Contract Members
**Constants & Variables**

```js
//private members
address private _owner;
mapping(address => uint8) private _stakeholders;
mapping(uint256 => struct MultiSig.Transaction) private _transactions;
uint256 private _transactionIndex;
uint256[] private _pendingTransactions;

//internal members
address payable internal addressToSendFundsTo;

//public members
uint256 public threshold;
uint256 public numberOfStakeholders;
uint256 public upgradeVariable;

```

**Events**

```js
event ThresholdEvent(uint256);
event TransactionData(string, address);
event TransactionSignatureCount(string, uint256);
event DepositFunds(address  from, uint256  amount);
event TransactionCreated(address  from, address  to, uint256  amount, uint256  transactionId);
event TransactionCompleted(address  from, address  to, uint256  amount, uint256  transactionId);
event TransactionSigned(address  by, uint256  transactionId);
event TransactionUnsigned(address  by, uint256  transactionId);
```

## Modifiers

- [isOwner](#isowner)
- [validStakeholder](#validstakeholder)

### isOwner

```js
modifier isOwner() internal
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### validStakeholder

```js
modifier validStakeholder() internal
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

## Functions

- [()](#)
- [()](#)
- [addStakeholder(address stakeholder)](#addstakeholder)
- [withdraw()](#withdraw)
- [pendingTransactions()](#pendingtransactions)
- [signTransaction(uint256 transactionId)](#signtransaction)
- [unsignTransaction(uint256 transactionId)](#unsigntransaction)
- [getThreshold()](#getthreshold)
- [contractBalance()](#contractbalance)
- [getNumberOfStakeholders()](#getnumberofstakeholders)
- [getTransactionVoteCount(uint256 transactionId)](#gettransactionvotecount)

### 

```js
function () public payable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### 

Default fallback function that emits an event and displays the address of the sender and the value passed

```js
function () external payable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### addStakeholder

This function allows for the contract owner to add new stakeholders in addition to calculating the minimum number of votes required to validate transactions (threshold).

```js
function addStakeholder(address stakeholder) public nonpayable isOwner 
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| stakeholder | address | The stakeholder address to be added | 

### withdraw

This function allows the contract caller to withdraw all the funds from the contract pending approval of one or more stakeholders (depending on the threshold value)

```js
function withdraw() public nonpayable validStakeholder 
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### pendingTransactions

This function allows to view the transaction indices that are pending validation/confirmation from a minimum number of stakeholders

```js
function pendingTransactions() public view validStakeholder 
returns(uint256[])
```

**Returns**

The transaction indices that are pending validation/confirmation from a minimal number of stakeholders

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### signTransaction

This function allows for a valid shareholder to sign/validate/confirm a specific transaction. A valid shareholder cannot sign a transaction more than once

```js
function signTransaction(uint256 transactionId) public nonpayable validStakeholder 
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| transactionId | uint256 | The transaction ID where a shareholder will sign/validate/confirm it | 

### unsignTransaction

This function allows for a valid shareholder to unsign/invalidate/unconfirm a specific transaction. A valid shareholder cannot unsign a transaction more than once

```js
function unsignTransaction(uint256 transactionId) public nonpayable validStakeholder 
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| transactionId | uint256 | The transaction ID where a shareholder will unsign/invalidate/unconfirm it | 

### getThreshold

A function to get the minimal number of votes required from stakeholders to approve a transaction (threshold)

```js
function getThreshold() public view
returns(uint256)
```

**Returns**

The threshold value

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### contractBalance

A read-only function that returns the balance of this contract

```js
function contractBalance() public view
returns(uint256)
```

**Returns**

This contract's balance

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### getNumberOfStakeholders

A read-only function that returns the total number of valid stakeholders

```js
function getNumberOfStakeholders() public view
returns(uint256)
```

**Returns**

The total number of valid stakeholders

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### getTransactionVoteCount

A function that returns the total vote count for a specific transaction

```js
function getTransactionVoteCount(uint256 transactionId) public view validStakeholder 
returns(uint256)
```

**Returns**

The total number of votes a transaction has

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| transactionId | uint256 | The transaction ID to see how many votes it has | 

## Contracts

* [Migrations](contracts/Exercise2.2/docs/docs/Migrations.md)
* [MultiSig](contracts/Exercise2.2/docs/MultiSig.mdse2.2/docs/MultiSig.md)
* [MultiSigV2](contracts/Exercise2.2/docs/MultiSigV2.md)
* [Ownable](contracts/Exercise2.2/docs/Ownable.md)
* [Physiotherapy](contracts/Exercise2.1/docs/PhysiotherapySmartContract.md)
* [SafeMath](contracts/Exercise2.2/docs/SafeMath.md)
* [StringUtils](contracts/Exercise2.1/docs/StringUtils.md)
* [UpgradeableContractProxy](contracts/Exercise2.2/docs/UpgradeableContractProxy.md)
