# UpgradableContractProxy
## constructor
## updateImplementation
*A function that allows the Proxy Smart Contract to point to a new Smart Contract address (new implementation)*

**Development notice:**
*This function allows to the Proxy Smart Contract to point to a new one, and assigns the _currentImplementation to be the new Smart Contract*


**Parameters:**
* _newImplementation `address`

## implementation
*A read-only function to view the address of the current implementation (current Smart Contract address)*
**Return Parameters:**
* `address`
## FALLBACK
*The default fallback function that uses complex assembly code to leverage the delegatecall opcode*

**Development notice:**
*A major drawback of this approach is that this function is reserved to leverage the delegatecall opcode.*

## isOwner

**Development notice:**
*Returns true if the caller is the current owner.*

**Return Parameters:**
* `bool`
