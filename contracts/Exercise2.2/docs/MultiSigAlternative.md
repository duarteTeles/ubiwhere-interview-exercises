# MultiSig
*Version 1 of a simple MultiSig Ethereum Smart Contract*
## constructor
## FALLBACK
## addStakeholder
*This function allows for the contract owner to add new stakeholders in addition to calculating the minimum number of votes required to validate transactions (threshold).*

**Development notice:**
*This function uses OpenZeppelin's SafeMath library to safely calculate the amount of minimum votes required to approve a transaction*


**Parameters:**
* stakeholder `address`: *The stakeholder address to be added*

## withdraw
*This function allows the contract caller to withdraw all the funds from the contract pending approval of one or more stakeholders (depending on the threshold value)*

**Development notice:**
*This function registers the transaction in a pending transactions array waiting for approval of a minimal amount of stakeholders (threshold)*

## pendingTransactions
*This function allows to view the transaction indices that are pending validation/confirmation from a minimum number of stakeholders*
**Return Parameters:**
* `uint[]`
## signTransaction
*This function allows for a valid shareholder to sign/validate/confirm a specific transaction. A valid shareholder cannot sign a transaction more than once*

**Development notice:**
*This function increments the number of stakeholder votes and checks if it equal or greater than the threshold. If it is,*


**Parameters:**
* transactionId `uint`: *The transaction ID where a shareholder will sign/validate/confirm it*

## unsignTransaction
*This function allows for a valid shareholder to unsign/invalidate/unconfirm a specific transaction. A valid shareholder cannot unsign a transaction more than once*

**Parameters:**
* transactionId `uint`: *The transaction ID where a shareholder will unsign/invalidate/unconfirm it*

## getThreshold
**Return Parameters:**
* `uint256`
## contractBalance
*A read-only function that returns the balance of this contract*
**Return Parameters:**
* `uint`
## getNumberOfStakeholders
*A read-only function that returns the total number of valid stakeholders*
**Return Parameters:**
* `uint256`
## getTransactionVoteCount
*A function that returns the total vote count for a specific transaction*

**Parameters:**
* transactionId `uint`: *The transaction ID to see how many votes it has*

**Return Parameters:**
* `uint256`
