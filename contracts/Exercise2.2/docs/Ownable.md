# Ownable - by OpenZeppelin (Ownable.sol)

View Source: [contracts/Exercise2.1/Ownable.sol](contracts/Exercise2.1/Ownable.sol)

**↘ Derived Contracts: [Physiotherapy](contracts/Exercise2.1/docs/PhysiotherapySmartContract.md), [UpgradeableContractProxy](contracts/Exercise2.2/docs/docsracts/Exercise2.2/docs/docs/UpgradeableContractProxy.md)**

**Ownable**

The Ownable contract has an owner address, and provides basic authorization control
functions, this simplifies the implementation of "user permissions".

## Contract Members
**Constants & Variables**

```js
address public owner;

```

## Modifiers

- [onlyOwner](#onlyowner)

### onlyOwner

Throws if called by any account other than the owner.

```js
modifier onlyOwner() internal
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

## Functions

- [()](#)
- [isOwner()](#isowner)

### 

Initializes the contract setting the deployer as the initial owner.

```js
function () public nonpayable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### isOwner

Returns true if the caller is the current owner.

```js
function isOwner() public view
returns(bool)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

## Contracts

* [Migrations](contracts/Exercise2.2/docs/docsracts/Exercise2.2/docs/docs/Migrations.md)
* [MultiSig](contracts/Exercise2.2/docs/docs/MultiSig.md)
* [MultiSigV2](contracts/Exercise2.2/docs/docsracts/Exercise2.2/docs/docs/MultiSigV2.md)
* [Ownable](contracts/Exercise2.2/docs/docsracts/Exercise2.2/docs/docs/Ownable.md)
* [Physiotherapy](contracts/Exercise2.1/docs/PhysiotherapySmartContract.md)
* [SafeMath](contracts/Exercise2.2/docs/docsracts/Exercise2.2/docs/docs/SafeMath.md)
* [StringUtils](contracts/Exercise2.1/docs/StringUtils.md)
* [UpgradeableContractProxy](contracts/Exercise2.2/docs/docsracts/Exercise2.2/docs/docs/UpgradeableContractProxy.md)
