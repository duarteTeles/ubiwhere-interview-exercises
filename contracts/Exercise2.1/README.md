# Introduction

This document describes the process of developing Ubiwhere's Internview Exercise 2.1: 
a physiotherapy center management solution using Ethereum Smart Contracts. This is detailed
in the chapter **Blockchain Software Engineering (Simplified)**, where all 
the most relevant software development phases are documented, albeit superficially.

# Blockchain Software Engineering (Simplified)

This chapter focuses on the software development process of Exercise 2.1 and the various
software phases required to develop safe, bug-free Ethereum Smart Contracts. These are:

1. **Requirement gathering and analysis**
2. **Design**
3. **Implementation**
4. **Testing**
5. **Deployment**

## Requirement Gathering and Analysis

This is the phase where system requirements are gathered an analyzed for the system/problem in question with the creation of multiple diagrams such as UML Use-Cases or Classes diagrams. Here, it will only be discussed the multiple functional requirements for Ubiwhere's Interview Exercise 2.1. 

### Functional Requirements

The functional requirements for this exercise are as follows:

1. Only an administrator can add/delete workers (in the exercise's case the admin is Manuela) by
providing his/her private key
2. When a worker is deleted by an admin, his/her activities are deleted as well
3. Only a worker can add or edit new activities and not an administrator
4. A worker must only add or edit his activities and no one else's

### Contracts Diagram

The chosen implementation has the following contracts diagram:

![The Contracts Diagram](./docs/images/Physiotherapy%20Contract%20Diagram.svg "The Contracts Diagram")

The main Smart Contract, **Physiotherapy.sol** inherits from the **Ownable** contract
and uses the **StringUtils** library, both by OpenZepplin (https://openzeppelin.com/contracts/).

In the diagram are also visible the structs, enums and functions it has.

### Design and Implementation

The design and implementation phase of developing software is where multiple aspects of this process are
defined such as technologies/frameworks to use, the folder structure of the application, etc.

In Exercise's 2.1 case, the Solidity version 0.5.0 was chosen, in addition to Truffle  version
5.0.0 and NodeJS version 11.15.0. The core Smart Contract, **Physiotherapy.sol**
was developed with no optimization in mind.

### Testing

Testing must be taken seriously when developing decentralized applications with Ethereum.
For this purpose, in the **test** folder, a **PhysiotherapyTest.js** unit test was developed
using Mocha syntax. Its test cases are the following:

1. It should be possible for only Manuela (administrator) to add workers: in this test,
a worker previously defined in the test is added in a test Blockchain (Ganache) by passing a test worker's private and public keypair
in addition to the administrator's private key and the funds a worker should have.
Then, we fetch the last worker added in the Blockchain and we see if it matches 
the preciously defined test worker. If it does, we proved a worker can be added to the test Blockchain.

2.  It should be possible for only workers to add activities to them: in this test, we attempt to
compare a previously defined activity for a specific worker with the one we add to the Blockchain
by invoking the contract's function **addActivityToWorker** and passing a valid worker's private and
public keypair for validation. If this is successful, we tested that only workers can add activities. 

3. It should be possible for only Manuela (administrator) to delete workers: we save the total worker array length
in a variable and invoke the **removeWorkerAtIndex** function from the contract to remove a worker. 
Then, we get the new workers array length. If they do not match, the test is successful

### Deployment

The deployment phase is one of the most important ones when developing Ethereum Smart Contracts,
as once a Smart Contract is deployed, it is immutable (however, there are Ethereum software patterns
such as the Proxy one that attempts to upgrade parts of a Smart Contract) and cannot be changed. 
For this reason, deployment must occur after the Smart Contracts are thoroughly tested before.

In the context of Exercise 2.1, it was deployed to the Rinkeby Ethereum Testnet to facilitate integration
from other physiotherapy companies (more on that in the next section). The deployed Smart Contract
is visible in EtherScan: https://rinkeby.etherscan.io/address/0xd6d9AC882308021886200efE57d52C95527b55a3#code.

## Company Integration of Physiotherapy Smart Contract

Any physiotherapy company can integrate the developed Smart Contracts. Explanation of a possible implementation is presented in the document [Integrate Physiotherapy Smart Contract.md](./docs/Integrate%20Physiotherapy%20Smart%20Contract.md). 

## Smart Contract Documentation

Smart Contract documentation about their methods, functions, etc can be found at the docs folder
in each exercise. For the main Smart Contract, this is  [PhysiotherapySmartContract.md](./docs/Integrate%20Physiotherapy%20Smart%20Contract.md) or [Physiotherapy Alternative.md](./docs/Physiotherapy%20Alternative.md).

## Limitations

Activity type is only an enum and does not take into account it should have structs depending
on this enum value. For this reason, structs **Aerossol**, **Current** and **UltraSound** are not used.
Also, the main Smart Contract was **NOT** developed by taking into account optimizations.

# Conclusion

This document documented briefly the whole software development process for Ubiwhere's Interview Exercise 2.1,  as well as its limitations and extra documentation. 