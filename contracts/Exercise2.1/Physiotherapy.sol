pragma solidity ^0.5.0;

import "./Ownable.sol";
import "./StringUtils.sol";


contract Physiotherapy is Ownable {

    /**
    * @title A simple contract to manage a physiotherapy center where an administrator can add and remove workers. Each worker can add/edit his/her activities
    * @author Duarte Teles, March 2020
    * @notice This is a simple contract to manage a physiotherapy center. It has a huge gas cost as it is not optimized
    * @dev This contract is not at all optimized. It has a high gas cost due to time limitations. Also, each activity type should be either
    * Ultra Sound, Current or Aerossol. However, due again to time limitations and a high gas cost, an activity type is just a simple enum.
    * Moreover, this contract takes advantage of as little cycles (for, while...) as possible. This contract is designed for looping of mappings/arrays to be done in the
    * client-side of an application that uses this contract
    */
    // The contract Owner
    address private _contractOwner;
    // The fixed administrator private key. Only the administrator can add or remove workers
    string private _adminPrivateKey = '0xf7cbd9813625157b42de1068b1d96bd7e5324821e3db384228226c0a752fdbc';

    // events
    event AddWorker(string privateKey, string publicKey, uint256 funds);
    event DeleteWorker(uint256 workerIndex);
    event DisplayWorkerData(string, string, uint256);

    // The ActivityType enum. With this value, we should be able to point to each corresponding struct. However, due to time
    // limitations, an Activity Type is always only an enum
    enum ActivityType {ULTRA_SOUND, CURRENT, AEROSSOL}

    // A mapping to have all the workers
    mapping(address => Worker[]) private _workers;

    // The Worker struct, where it has a pair of private/public keys added by an administrator and funds to spend on their activities
    // For simplicity, the funds are represented as an integer. In addition to this, each worker has a mapping of his/her activities
    struct Worker {
        string privateKey;
        string publicKey;
        uint256 funds;
        mapping(address => Activity[]) activities;
    }


    // The struct of activity. ActivityType, as previously mentioned, is only an enum
    struct Activity
    {
        string startDate;
        string endDate;
        string description;
        ActivityType activityType;
    }

    // UltraSound, Currentand Aerossol structs are only here defined but are not used as previously explained
    struct UltraSound
    {
        uint power;
        uint frequency;
    }

    struct Current
    {
        uint diameter;
        uint humidity;
    }

    struct Aerossol
    {
        uint quantity;
        uint ventilan;
    }

    // A simple modifier that matches a private key to the one defined in this contract. If it is equal, then access is granted as an administrator.
    /**
    @param privateKey A private key to see if an address is an administrator
    */
    modifier isAdmin(string  memory privateKey)
    {
        require(
            StringUtils.equal(privateKey, _adminPrivateKey), "Only the Administrator Can Add, Delete or View Workers"
        );

        _;
    }

    // On the other hand, if it is not an administrator, we need to check if it is a worker.
    // This is done by passing a privateKey and see if it matches the administrator's private key.
    // If it does not match, then automatically is a worker

    /**
    @param privateKey A private key to see if an address is a worker
    */
    modifier isWorker(string  memory privateKey)
    {
        require(
            !StringUtils.equal(privateKey, _adminPrivateKey), "Must be a worker, not an Administrator."
        );

        _;
    }

    // Constructor
    constructor() public {
        _contractOwner = msg.sender;
    }

    /**
 * @author Duarte Teles
 * @notice A simple function to return the number of workers
 * @dev This function is crucial for any client-side consuming this contract to loop through the workers mapping.
 * This function should be used in combination with getWorkerAtIndex and getWorkerActivityAtIndex
 * @return The length of the workers mapping
 */
    function getWorkersLength() public view returns (uint256)
    {
        return _workers[_contractOwner].length;
    }

    /**
    * @author Duarte Teles
    * @notice A simple function to return the number of activities for a given worker
    * @dev This function is crucial for any client-side consuming this contract to loop through the activities mapping.
    * This function should be used in combination with getWorkerAtIndex and getWorkerActivityAtIndex
    * @param workerIndex The index of the worker
    * @return The length of the activities mapping for a specific worker
    */
    function getWorkerActivitiesLength(uint256 workerIndex) public view returns (uint256)
    {
        return _workers[_contractOwner][workerIndex].activities[_contractOwner].length;
    }


    /**
    * @author Duarte Teles
    * @notice This function allows for the contract owner to add a new worker if it is an administrator
    * @dev This function receives a privateKey and uses the isAdmin modifier to check if the contract owner is an administrator. If that's true,
    * then the worker private and public keypair is added to the _workers mapping
    * @param adminPrivateKey The administrator private key
    * @param workerPrivateKey The worker private key to add
    * @param workerPublicKey The worker public key to add
    * @param funds The number of funds a worker has
    */
    function addWorker
    (string memory adminPrivateKey,
        string memory workerPrivateKey,
        string memory workerPublicKey,
        uint256 funds)
    public isAdmin(adminPrivateKey) {

        Worker memory worker = Worker(workerPrivateKey, workerPublicKey, funds);
        _workers[_contractOwner].push(worker);

        emit AddWorker(workerPrivateKey, workerPublicKey, funds);
    }


    /**
   * @author Duarte Teles
   * @notice This function allows for the contract owner to remove a worker based on its index and if it is an administrator. It also removes his/her activities if there are any
   * @dev This function receives a privateKey and uses the isAdmin modifier to check if the contract owner is an administrator. If that's true,
   * then the worker is removed from the worker mapping at the index passed by parameter. Note that this function does not check if the worker index
   * passed as parameter exists. If it does not, then invoking this function will fail. It is the responsibility of the client-side of the contract
   * caller to account for this
   * @param workerIndex The worker index to remove
   * @param adminPrivateKey The administrator private key
   */
    function removeWorkerAtIndex(uint256 workerIndex, string memory adminPrivateKey)
    public isAdmin(adminPrivateKey) {

        // First we check if there are activities for that specific worker. If there are, we proceed to delete all of them
        if (_workers[_contractOwner][workerIndex].activities[_contractOwner].length > 0)
        {
            delete _workers[_contractOwner][workerIndex].activities[_contractOwner];
        }

        // After deleting his/her activities, we then proceed to delete a worker from the workers mapping
        delete _workers[_contractOwner][workerIndex];
        // Since a worker is deleted, the length of the workers mapping is decreased and an event is emitted
        _workers[_contractOwner].length--;
        emit DeleteWorker(workerIndex);

    }

    /**
   * @author Duarte Teles
   * @notice This function returns a worker's properties if the private key passed is a valid administrator
   * @dev This function is designed to return one worker element at a time. This is to avoid even higher gas costs
   * due to looping through the dynamic mapping. A client that consumes this contract should use this function to get all the
   * workers
   * @param workerIndex The worker index
   * @param adminPrivateKey The administrator private key
   */
    function getWorkerAtIndex(
        uint8 workerIndex,
        string memory adminPrivateKey) isAdmin(adminPrivateKey)
    public returns (
        string memory,
        string memory,
        uint256)
    {

        Worker storage worker = _workers[_contractOwner][workerIndex];

        emit DisplayWorkerData(worker.privateKey, worker.publicKey, worker.funds);
        return (worker.privateKey, worker.publicKey, worker.funds);
    }

    /**
    * @author Duarte Teles
    * @notice This function returns a worker's specific activity for a predetermined worker
    * @dev This function requires the worker index and activity index to be passed as parameter. This is
    * for clients consuming this contract to be able to do all the looping through dynamic arrays and save substantially
    * on gas costs. It returns all the data from a worker's activities in addition to a worker's public key so it
    * becomes easier on the client-side to visualize which activities belong to each worker
    * @param workerIndex The worker index
    * @param activityIndex The activity index
    * @return The worker's public key;
    * The start date of a specific worker's activity;
    * The end date of a specific worker's activity;
    * The description of a specific worker's activity;
    * The activity type of a specific worker's activity (an enum only)
    */
    function getWorkerActivityAtIndex
    (uint8 workerIndex,
        uint8 activityIndex)
    public view returns (
        string memory,
        string memory,
        string memory,
        string memory,
        ActivityType)
    {

        Worker storage worker = _workers[_contractOwner][workerIndex];

        return (
        worker.publicKey,
        worker.activities[_contractOwner][activityIndex].startDate,
        worker.activities[_contractOwner][activityIndex].endDate,
        worker.activities[_contractOwner][activityIndex].description,
        worker.activities[_contractOwner][activityIndex].activityType);
    }


    /**
    * @author Duarte Teles
    * @notice A function to add an activity to a worker
    * @dev This function adds an activity for a specific worker with its index passed as parameter if the contract caller is not an administrator and
    * the private/public keypair matches a valid worker. This is done by
    * using OpenZeppelin's StringUtils library
    * @param workerIndex The worker index to add an activity to
    * @param _workerPrivateKey The worker's private key
    * @param _workerPublicKey The worker's public key
    * @param _activityType The activity type (enum, 0, 1 or 2 only)
    * @param _startDate The activity's start date
    * @param _endDate The activity's end date
    * @param _description The activity's description
    */
    function addActivityToWorker(
        uint8 workerIndex,
        string memory _workerPrivateKey,
        string memory _workerPublicKey,
        ActivityType _activityType,
        string memory _startDate,
        string memory _endDate,
        string memory _description)
    public isWorker(_workerPrivateKey) {


        // There must be workers to add activities to
        require(_workers[_contractOwner].length > 0, "Workers cannot be empty!");

        // A comparison is performed using OpenZeppelin's StringUtils library to see if the private/public keypair passed as parameter matches
        // a valid owner. A valid owner can only add/edit his own activities and nobody else's.
        require(
            StringUtils.equal(_workers[_contractOwner][workerIndex].privateKey, _workerPrivateKey) &&
            StringUtils.equal(_workers[_contractOwner][workerIndex].publicKey, _workerPublicKey), "You can only add activities to yourself."
        );

        // Creation of an activity in memory with data passed by parameter
        Activity memory activity = Activity(_startDate, _endDate, _description, _activityType);

        // A necessary conversion from activity type to enum, so we can perform a check to see if the activity type parameter is valid
        uint8 activity_option = uint8(_activityType);
        require(activity_option == 0 || activity_option == 1 || activity_option == 2, "Activity Type must be 0, 1 or 2!");

        // Adding an activity to a specific worker
        _workers[_contractOwner][workerIndex].activities[_contractOwner].push(activity);

    }

    /**
    * @author Duarte Teles
    * @notice A function to edit an activity from a specific worker
    * @dev This function edits an activity for a specific worker with its index passed as parameter if the contract caller is not an administrator and
    * the private/public keypair matches a valid worker. This is done by
    * using OpenZeppelin's StringUtils library
    * @param workerIndex The worker index to edit an activity from
    * @param _workerPrivateKey The worker's private key
    * @param _workerPublicKey The worker's public key
    * @param _activityType The activity type (enum, 0, 1 or 2 only)
    * @param _startDate The activity's start date
    * @param _endDate The activity's end date
    * @param _description The activity's description
    * @param activityIndex The activity index to edit from
    */
    function editActivityFromWorker(
        uint256 workerIndex,
        string memory _workerPrivateKey,
        string memory _workerPublicKey,
        ActivityType _activityType,
        string memory _startDate,
        string memory _endDate,
        string memory _description,
        uint256 activityIndex)
    public isWorker(_workerPrivateKey) {

        // There must be workers to edit activities from
        require(_workers[_contractOwner].length > 0, "Workers cannot be empty!");


        // A comparison is performed using OpenZeppelin's StringUtils library to see if the private/public keypair passed as parameter matches
        // a valid owner. A valid owner can only add/edit his own activities and nobody else's.
        require(
            StringUtils.equal(_workers[_contractOwner][workerIndex].privateKey, _workerPrivateKey) &&
            StringUtils.equal(_workers[_contractOwner][workerIndex].publicKey, _workerPublicKey),
            "You can only edit activities to yourself."
        );

        // Creation of an activity in memory with data passed by parameter
        Activity memory activity = Activity(_startDate, _endDate, _description, _activityType);

        uint8 activity_option = uint8(_activityType);
        require(activity_option == 0 || activity_option == 1 || activity_option == 2,
            "Activity Type must be 0, 1 or 2!");

        // Editing the activity
        _workers[_contractOwner][workerIndex].activities[_contractOwner][activityIndex] = activity;
    }
}