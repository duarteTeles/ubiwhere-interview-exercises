# Introduction


This document describes the steps a company could take to integrate the Physiotherapy Smart
Contract into an existing web3 compatible web/mobile application. In this document, the Truffle
framework is used. There are code snippets on how to implement core functionalities
such as adding/removing workers and adding/editing worker activities in a Truffle application and using
Metamask configured to the Ethereum Rinkeby Testnet.

## Prerequisites

In order to integrate the Physiotherpay Smart Contract, one needs to have:

1. A Web3 compatible framework such as Truffle
2. An Ethereum Rinkeby Testnet node or Metamask
3. Access to the Physiotherapy Smart Contract at address on the Rinkeby Testnet
**0xd6d9AC882308021886200efE57d52C95527b55a3** 
(https://rinkeby.etherscan.io/address/0xd6d9AC882308021886200efE57d52C95527b55a3#code)

## How Integrate the Physiotherapy Ethereum Smart Contract

There are many steps required for a company to integrate the Physiotherapy Smart Contract. For
the purpose of this guide, it will be assumed that you have the **Truffle** framework
installed and you are using the **Metamask** Ethereum Wallet configured for the
**Rinkeby** testnet. We will break the integration by core functionalities of the Smart Contract.

It also assumed that you have access to the Rinkeby Physiotherapy Smart Contract address
(https://rinkeby.etherscan.io/address/0xd6d9AC882308021886200efE57d52C95527b55a3#code)
and can browse its code and interact with it.


### Adding a Worker

 Adding a worker is simple: all we require is a private key of the only person
 that can add workers (Manuela). This private key is: 
 **0xf7cbd9813625157b42de1068b1d96bd7e5324821e3db384228226c0a752fdbc**. 

 **Note**: The private key mentioned above will be mentioned throughout this document
 as the **administrator private key**.
 
 Now that we have the administrator private key (Manuela's private key), 
 we can proceed to interact with the Physiotherapy Smart Contract using Truffle and Web3. For exemple, in Truffle we 
 could have a JavaScript file that performs the actions described in this document and has some form of
 user interaction using Metamask configured form the Rinkeby testnet.

In order to perform any interaction with the contract such as adding a worker, we need to have a physiotherapy contract instance pointing to the address mentioned above
 (**0xd6d9AC882308021886200efE57d52C95527b55a3**)
 as well as the contract's ABI. The latter can be fetched from EtherScan. For example:
 

```javascript

const PhysiotherapyContract = await web3.eth.contract(<Physiotherapy Contract ABI Fetched From EtherScan>);
const physiotherapyContractInstance = await PhysiotherapyContract.at(0xd6d9AC882308021886200efE57d52C95527b55a3);

```

 We can now proceed to add as many workers as we like with the **addWorker** function of the smart contract and passing the following parameters:
 
 1. **admin privateKey**: the administrator's administrator private key (**0xf7cbd9813625157b42de1068b1d96bd7e5324821e3db384228226c0a752fdbc**)
 2. **workerPrivateKey**: a new worker's private key
 3. **workerPublicKey**: a new worker's public key
 4. **funds**: the number of funds a worker has to spend on the Ethereum Blockchain
 
 Example:
 
 ```javascript
 
// Adding workers:

await <contract instance>.addWorker("0xf7cbd9813625157b42de1068b1d96bd7e5324821e3db384228226c0a752fdbc", "0x0", "0x0", 100)>
 ```
 
 After adding as many workers as we would like, we can now proceed to loop through the workers array. To this, we first need to get the number of workers (workers array length).
This number is especially useful in the frontend to loop through the workers array. We achieve this by invoking the **getWorkersLength** function.
The final step is to view workers at a given index by using the **getWorkerAtIndex** function and supplying the administrator private key. This combined with the **getWorkersLength**, allows
for one to loop through the workers array and perform validations if required. Example:

```javascript

const workersArrayLength = await <contract instance>.getWorkersLength>
for (let i =0; i<workersArrayLength; i++)
{
const worker = await <contract instance>.getWorkerAtIndex(i, <administrator private key>)
console.log(worker.privateKey);
...
}
```

### Removing a Worker

 Removing a worker is just as simple as adding one. The only difference is that alongside with the worker, all his activities are deleted as well (if he has any).
 
 Programmatically, this is the same as adding a worker:
 
 ```javascript
 
 const workersArrayLength = await <contract instance>.getWorkersLength>
 for (let i =0; i<workersArrayLength; i++)
 {
 await <contract instance>.removeWorkerAtIndex(i, <administrator private key>)
 ...
 }
 ```

### Adding an activity to a Worker

Only workers can add activities and only to themselves. No administrator can perform this task.

With that said, the process of adding an activity to a worker is simple:

1. Loop through the workers array like before:

 ```javascript
 
 const workersArrayLength = <contract instance>.getWorkersLength>
 for (let i =0; i<workersArrayLength; i++)
 {
 const worker = <contract instance>.getWorkerAtIndex(i, <administrator private key>)


 }
 ```

2. Inside the loop, add the required parameters to the **addActivitytoWorker** function:

* **workerIndex**: the index you adquire from looping through the workers array
* **workerPrivateKey**: a valid administrator's private key. Note: here you can loop through the worker's
array to figure out there's a worker with a valid private/public keypair
* **workerPublicKey**: a valid worker's public key
* **activityType**: the activity type, only accepts 0, 1 or 2 (the activityType was supposed to be one of current, aerossol or ultra sound, but this was not implemented due to time constraints mainly)
* **startDate**: the activity start date
* **endDate**: the activity end date
* **description**: the activity description

Example: 

 ```javascript
 
 const workersArrayLength = await <contract instance>.getWorkersLength>
 for (let i =0; i<workersArrayLength; i++)
 {
 const worker = await <contract instance>.getWorkerAtIndex(i, <administrator private key>)
// Here checks can be made to see if a worker's private and public keypairs exist:
if (worker.privateKey = "0x0" && worker.publicKey == "0x0")
{
 await <contract instance>.addActivityToWorker(i, "0x0", worker.privateKey, worker.publicKey, 1, new Date(), new Date(), "description" )
}

 }
 ```

### Editing an activity from a Worker

Editing an activity from a worker is almost the same as adding an activity. The only difference is we need to pass the activity we want to edit and we need
to get the activity we want to edit first. We do this by invoking the function **getWorkerActivitiesLength** and passing the worker index. 
Then, we can find the activity we want to edit from the function **getWorkerActivityAtIndex**. Example:

 ```javascript
 
 const workersArrayLength = await <contract instance>.getWorkersLength>
 for (let i =0; i<workersArrayLength; i++)
 {
 const worker = await <contract instance>.getWorkerAtIndex(i, <administrator private key>)
// Here checks can be made to see if a worker's private and public keypairs exist:
if (worker.privateKey = "0x0" && worker.publicKey == "0x0")
{
// The difference is now we want to get all of a worker's activities. First we invoke the getWorkerActivitiesLength to figure out how many activities this specific user has
// and we pass the worker index

const workerActivitiesLength =  await <contract instance>.getWorkerActivitiesLength(i);
// Now we loop to find all the activities from this user from an activity we want to edit:


 for (let j =0; i<workerActivitiesLength; j++)
 {
 const workerActivities = await <contract instance>.getWorkerActivityAtIndex(i)>
// Optional activity index filtering here, we will edit the first entry

 await <contract instance>.editActivityFromWorker(i, "0x0", worker.privateKey, worker.publicKey, 1, new Date(), new Date(), "description", 0)>

}

 }
 ```