# Physiotherapy.sol

View Source: [contracts/Exercise2.1/Physiotherapy.sol](contracts/Exercise2.1/Physiotherapy.sol)

**↗ Extends: [Ownable](contracts/Exercise2.2/docs/docs/Ownable.md)**

**Physiotherapy**

**Enums**
### ActivityType

```js
enum ActivityType {
 ULTRA_SOUND,
 CURRENT,
 AEROSSOL
}
```

## Structs
### Worker

```js
struct Worker {
 string privateKey,
 string publicKey,
 uint256 funds,
 mapping(address => struct Physiotherapy.Activity[]) activities
}
```

### Activity

```js
struct Activity {
 string startDate,
 string endDate,
 string description,
 enum Physiotherapy.ActivityType activityType
}
```

### UltraSound

```js
struct UltraSound {
 uint256 power,
 uint256 frequency
}
```

### Current

```js
struct Current {
 uint256 diameter,
 uint256 humidity
}
```

### Aerossol

```js
struct Aerossol {
 uint256 quantity,
 uint256 ventilan
}
```

## Contract Members
**Constants & Variables**

```js
address private _contractOwner;
string private _adminPrivateKey;
mapping(address => struct Physiotherapy.Worker[]) private _workers;

```

**Events**

```js
event AddWorker(string  privateKey, string  publicKey, uint256  funds);
event DeleteWorker(uint256  workerIndex);
event DisplayWorkerData(string, string, uint256);
```

## Modifiers

- [isAdmin](#isadmin)
- [isWorker](#isworker)

### isAdmin

```js
modifier isAdmin(string privateKey) internal
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| privateKey | string | A private key to see if an address is an administrator | 

### isWorker

```js
modifier isWorker(string privateKey) internal
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| privateKey | string | A private key to see if an address is a worker | 

## Functions

- [()](#)
- [getWorkersLength()](#getworkerslength)
- [getWorkerActivitiesLength(uint256 workerIndex)](#getworkeractivitieslength)
- [addWorker(string adminPrivateKey, string workerPrivateKey, string workerPublicKey, uint256 funds)](#addworker)
- [removeWorkerAtIndex(uint8 workerIndex, string adminPrivateKey)](#removeworkeratindex)
- [getWorkerAtIndex(uint8 workerIndex, string adminPrivateKey)](#getworkeratindex)
- [getWorkerActivityAtIndex(uint8 workerIndex, uint8 activityIndex)](#getworkeractivityatindex)
- [addActivityToWorker(uint8 workerIndex, string _workerPrivateKey, string _workerPublicKey, enum Physiotherapy.ActivityType _activityType, string _startDate, string _endDate, string _description)](#addactivitytoworker)
- [editActivityFromWorker(uint256 workerIndex, string _workerPrivateKey, string _workerPublicKey, enum Physiotherapy.ActivityType _activityType, string _startDate, string _endDate, string _description, uint256 activityIndex)](#editactivityfromworker)

### 

```js
function () public nonpayable
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### getWorkersLength

A simple function to return the number of workers

```js
function getWorkersLength() public view
returns(uint256)
```

**Returns**

The length of the workers mapping

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|

### getWorkerActivitiesLength

A simple function to return the number of activities for a given worker

```js
function getWorkerActivitiesLength(uint256 workerIndex) public view
returns(uint256)
```

**Returns**

The length of the activities mapping for a specific worker

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| workerIndex | uint256 | The index of the worker | 

### addWorker

This function allows for the contract owner to add a new worker if it is an administrator

```js
function addWorker(string adminPrivateKey, string workerPrivateKey, string workerPublicKey, uint256 funds) public nonpayable isAdmin 
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| adminPrivateKey | string | The administrator private key | 
| workerPrivateKey | string | The worker private key to add | 
| workerPublicKey | string | The worker public key to add | 
| funds | uint256 | The number of funds a worker has. It is to replicate that each user has X amount of Ether | 

### removeWorkerAtIndex

This function allows for the contract owner to remove a worker based on its index and if it is an administrator. It also removes his/her activities if there are any

```js
function removeWorkerAtIndex(uint8 workerIndex, string adminPrivateKey) public nonpayable isAdmin 
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| workerIndex | uint8 | The worker index to remove | 
| adminPrivateKey | string | The administrator private key | 

### getWorkerAtIndex

This function returns a worker's properties if the private key passed is a valid administrator

```js
function getWorkerAtIndex(uint8 workerIndex, string adminPrivateKey) public nonpayable isAdmin 
returns(string, string, uint256)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| workerIndex | uint8 | The worker index | 
| adminPrivateKey | string | The administrator private key | 

### getWorkerActivityAtIndex

This function returns a worker's specific activity for a predetermined worker

```js
function getWorkerActivityAtIndex(uint8 workerIndex, uint8 activityIndex) public view
returns(string, string, string, string, enum Physiotherapy.ActivityType)
```

**Returns**

The worker's public key;
The start date of a specific worker's activity;
The end date of a specific worker's activity;
The description of a specific worker's activity;
The activity type of a specific worker's activity (an enum only)

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| workerIndex | uint8 | The worker index | 
| activityIndex | uint8 | The activity index | 

### addActivityToWorker

A function to add an activity to a worker

```js
function addActivityToWorker(uint8 workerIndex, string _workerPrivateKey, string _workerPublicKey, enum Physiotherapy.ActivityType _activityType, string _startDate, string _endDate, string _description) public nonpayable isWorker 
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| workerIndex | uint8 | The worker index to add an activity to | 
| _workerPrivateKey | string | The worker's private key | 
| _workerPublicKey | string | The worker's public key | 
| _activityType | enum Physiotherapy.ActivityType | The activity type (enum, 0, 1 or 2 only) | 
| _startDate | string | The activity's start date | 
| _endDate | string | The activity's end date | 
| _description | string | The activity's description | 

### editActivityFromWorker

A function to edit an activity from a specific worker

```js
function editActivityFromWorker(uint256 workerIndex, string _workerPrivateKey, string _workerPublicKey, enum Physiotherapy.ActivityType _activityType, string _startDate, string _endDate, string _description, uint256 activityIndex) public nonpayable isWorker 
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| workerIndex | uint256 | The worker index to edit an activity from | 
| _workerPrivateKey | string | The worker's private key | 
| _workerPublicKey | string | The worker's public key | 
| _activityType | enum Physiotherapy.ActivityType | The activity type (enum, 0, 1 or 2 only) | 
| _startDate | string | The activity's start date | 
| _endDate | string | The activity's end date | 
| _description | string | The activity's description | 
| activityIndex | uint256 | The activity index to edit from | 

## Contracts

* [Migrations](contracts/Exercise2.2/docs/docs/Migrations.md)
* [MultiSig](contracts/Exercise2.2/docs/MultiSig.md)
* [MultiSigV2](contracts/Exercise2.2/docs/MultiSigV2.md)
* [Ownable](contracts/Exercise2.2/docs/Ownable.md)
* [Physiotherapy](contracts/Exercise2.1/docs/PhysiotherapySmartContract.md/docs/Physiotherapy.md)
* [SafeMath](contracts/Exercise2.2/docs/SafeMath.md)
* [StringUtils](contracts/Exercise2.1/docs/StringUtils.md)
* [UpgradeableContractProxy](docsd)
