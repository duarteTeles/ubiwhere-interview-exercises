# Physotheraphy
## constructor
## getWorkersLength
*A simple function to return the number of workers*

**Development notice:**
*This function is crucial for any client-side consuming this contract to loop through the workers mapping.*

**Return Parameters:**
* `uint256`
## getWorkerActivitiesLength
*A simple function to return the number of activities for a given worker*

**Development notice:**
*This function is crucial for any client-side consuming this contract to loop through the activities mapping.*


**Parameters:**
* workerIndex `uint256`: *The index of the worker*

**Return Parameters:**
* `uint256`
## addWorker
*This function allows for the contract owner to add a new worker if it is an administrator*

**Development notice:**
*This function receives a privateKey and uses the isAdmin modifier to check if the contract owner is an administrator. If that's true,*


**Parameters:**
* adminPrivateKey `string`: *The administrator private key*
* workerPrivateKey `string`: *The worker private key to add*
* workerPublicKey `string`: *The worker public key to add*
* funds `uint256`: *The number of funds a worker has*

## removeWorkerAtIndex
*This function allows for the contract owner to remove a worker based on its index and if it is an administrator. It also removes his/her activities if there are any*

**Development notice:**
*This function receives a privateKey and uses the isAdmin modifier to check if the contract owner is an administrator. If that's true,*


**Parameters:**
* workerIndex `uint256`: *The worker index to remove*
* adminPrivateKey `string`: *The administrator private key*

## getWorkerAtIndex
*This function returns a worker's properties if the private key passed is a valid administrator*

**Development notice:**
*This function is designed to return one worker element at a time. This is to avoid even higher gas costs*


**Parameters:**
* workerIndex `uint8`: *The worker index*
* adminPrivateKey `string`: *The administrator private key*

**Return Parameters:**
* `string`
* `string`
* `uint256`
## getWorkerActivityAtIndex
*This function returns a worker's specific activity for a predetermined worker*

**Development notice:**
*This function requires the worker index and activity index to be passed as parameter. This is*


**Parameters:**
* workerIndex `uint8`: *The worker index*
* activityIndex `uint8`: *The activity index*

**Return Parameters:**
* `string`
* `string`
* `string`
* `string`
* `ActivityType`
## addActivityToWorker
*A function to add an activity to a worker*

**Development notice:**
*This function adds an activity for a specific worker with its index passed as parameter if the contract caller is not an administrator and*


**Parameters:**
* workerIndex `uint8`: *The worker index to add an activity to*
* _workerPrivateKey `string`: *The worker's private key*
* _workerPublicKey `string`: *The worker's public key*
* _activityType `ActivityType`: *The activity type (enum, 0, 1 or 2 only)*
* _startDate `string`: *The activity's start date*
* _endDate `string`: *The activity's end date*
* _description `string`: *The activity's description*

## editActivityFromWorker
*A function to edit an activity from a specific worker*

**Development notice:**
*This function edits an activity for a specific worker with its index passed as parameter if the contract caller is not an administrator and*


**Parameters:**
* workerIndex `uint256`: *The worker index to edit an activity from*
* _workerPrivateKey `string`: *The worker's private key*
* _workerPublicKey `string`: *The worker's public key*
* _activityType `ActivityType`: *The activity type (enum, 0, 1 or 2 only)*
* _startDate `string`: *The activity's start date*
* _endDate `string`: *The activity's end date*
* _description `string`: *The activity's description*
* activityIndex `uint256`: *The activity index to edit from*

## isOwner

**Development notice:**
*Returns true if the caller is the current owner.*

**Return Parameters:**
* `bool`
