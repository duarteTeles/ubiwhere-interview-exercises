# StringUtils.sol

View Source: [contracts/Exercise2.1/StringUtils.sol](contracts/Exercise2.1/StringUtils.sol)

**StringUtils**

## Functions

- [compare(string _a, string _b)](#compare)
- [equal(string _a, string _b)](#equal)
- [indexOf(string _haystack, string _needle)](#indexof)

### compare

Does a byte-by-byte lexicographical comparison of two strings.

```js
function compare(string _a, string _b) public nonpayable
returns(int256)
```

**Returns**

a negative number if `_a` is smaller, zero if they are equal
 and a positive numbe if `_b` is smaller.

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _a | string |  | 
| _b | string |  | 

### equal

Compares two strings and returns true iff they are equal.

```js
function equal(string _a, string _b) public nonpayable
returns(bool)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _a | string |  | 
| _b | string |  | 

### indexOf

Finds the index of the first occurrence of _needle in _haystack

```js
function indexOf(string _haystack, string _needle) public nonpayable
returns(int256)
```

**Arguments**

| Name        | Type           | Description  |
| ------------- |------------- | -----|
| _haystack | string |  | 
| _needle | string |  | 

## Contracts

* [Migrations](contracts/Exercise2.2/docs/docs/Migrations.md)
* [MultiSig](contracts/Exercise2.2/docs/docs/MultiSig.md)
* [MultiSigV2](contracts/Exercise2.2/docs/docs/MultiSigV2.md)
* [Ownable](contracts/Exercise2.2/docs/docs/Ownable.md)
* [Physiotherapy](contracts/Exercise2.1/docs/PhysiotherapySmartContract.md/docs/Physiotherapy.md)
* [SafeMath](contracts/Exercise2.2/docs/docs/SafeMath.md)
* [StringUtils](contracts/Exercise2.1/docs/StringUtils.md.1/docs/StringUtils.md)
* [UpgradeableContractProxy](contracts/Exercise2.2/docs/docs/UpgradeableContractProxy.md)
