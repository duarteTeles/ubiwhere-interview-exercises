const Physiotherapy = artifacts.require("./Physiotherapy.sol");
const StringUtils = artifacts.require("./StringUtils.sol");
const MultiSig = artifacts.require("./MultiSig.sol");
const MultiSigV2 = artifacts.require("./MultiSigV2.sol");
const Ownable = artifacts.require("./Ownable.sol");
const SafeMath = artifacts.require("./SafeMath.sol");
const UpgradeableContractProxy = artifacts.require("./UpgradeableContractProxy.sol");

module.exports = function (deployer) {

    deployer.deploy(StringUtils);
    deployer.link(StringUtils, Physiotherapy);
    deployer.deploy(Physiotherapy);
    deployer.deploy(SafeMath);
    deployer.link(SafeMath, MultiSig);
    deployer.deploy(MultiSig);
    deployer.deploy(Ownable);
    deployer.deploy(MultiSigV2);
    deployer.deploy(UpgradeableContractProxy);
};
