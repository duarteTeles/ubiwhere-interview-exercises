const path = require("path");
const HDWalletProvider = require("truffle-hdwallet-provider");

const mnemonicInfura = process.env.mnemonicInfura;
const infuraAPIKey = process.env.infuraAPIKey;

module.exports = {
    contracts_build_directory: path.join(__dirname, "build/contracts"),
    networks: {
        development: {
            host: "127.0.0.1",
            port: 7545,
            network_id: "*",
        },
        rinkeby: {
            provider: function () {
                return new HDWalletProvider(
                    mnemonicInfura,
                    'https://rinkeby.infura.io/' + infuraAPIKey
                )
            },
            network_id: 4

        },
        compilers: {
            solc: {
                version: "0.5.0",
                settings: {
                    optimizer: {
                        enabled: true
                    }
                }
            }
        }
    }
};
