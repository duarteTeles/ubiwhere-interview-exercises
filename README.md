# Uniwhere Interview Exercise Solutions

This is the main README for the Uniwhere Interview Exercises project. It contains all the necessary instructions for anyone to setup and test what was developed for these exercises.

## Getting Started

THe following sections describe how anyone can get this project setup and running.

### Prerequisites

The following is required to be installed on your machine:

1. Node version 11.15.0 or greater
2. Truffle version 5.0.0 installed
3. Ganache version v2.1.2 or greater


### Setup and Installation

To setup up this project, first clone the project:

```
git clone https://duarteTeles@bitbucket.org/duarteTeles/ubiwhere-interview-exercises.git
```

Then navigate to the project folder and install all the required dependencies:

```
cd ubiwhere-interview-exercises && npm install
```

Install Truffle version 5.0.0 (if it not installed already):

```
npm install -g truffle@5.0.0
```

Check if Truffle was installed correctly by invoking Truffle:

```
truffle version
```

It should print something like this:

```
Truffle v5.0.0 (core: 5.0.0)
Solidity v0.5.0 (solc-js)
Node v11.15.0
```

Now compile all the Ethereum Smart Contracts:

```
truffle compile
```

By default, to compile the Smart Contracts, Truffle requires that Ganache is running on port 7545. You can change this in file **truffle-config.js**.

## Folder Structure

**build** (generated only): contains the ABIs of the compiled Ethereum Smart Contracts by Truffle  
**contracts**: contains the source code of the Ethereum Smart Contracts used for both exercises  
**contracts/Exercise2.1**: contains a README.md which explains the process behind developing   
this exercise in more detail, in addition to having the source code of the   
Ethereum Smart Contracts used in this exercise and all the relevant documentation  
**contracts/Exercise2.2**: contains a README.md which explains the process behind developing   
this exercise in more detail, in addition to having the source code of the Ethereum   
Smart Contracts used in this exercise and all the relevant documentation  
**migrations**: contains the necessary migrations Truffle uses when deploying Smart Contracts  
**test**: contains all the tests used in both exercises  

## Testing the Smart Contracts

Truffle has integrated test running. To run them, use the following command:

```
truffle compile && truffle test
```

All tests should pass and they are documented in each README.md in each exercise folder (example: Exercise2.1/README.md).

**Note**: when running tests, do not forget to have Ganache running on port **7545**.


## Deployment

Deploying the developed Smart Contracts requires that the Truffle migrations work without a problem and that the file **truffle-config** is configured with instructions to deploy on the network of choice (mainnet, Rinkeby, Kovan...), in addition to a running Ethereum node to the chosen network. In this project, Infura was chosen as the Rinkeby Ethereum node.    
  
Deployment command:
  
  ```
truffle migrate &&  truffle deploy --network <network>
```
## Built With

* [Truffle](https://www.trufflesuite.com/) - A Framework to develop Ethereum Smart Contracts
* [Solidity](https://solidity.readthedocs.io/en/v0.6.4/) - An Ethereum language
* [Ganache](https://www.trufflesuite.com/ganache) - A private and local Ethereum Blockchain
* [Infura](https://infura.io/) - Ethereum and IPFS APIs

## Authors

* **Duarte Teles** - [Bitbucket](https://bitbucket.org/%7B65e86de9-c623-412d-8584-596b396bbb67%7D/)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

I would like to thank Ubiwhere for this amazing opportunity to show my value. I have had a lot of fun developing this project.

